/*
	What is Node.js?

	NodeJS is a runtime environment which allows us to create/develop backend/server-side applications with javascript.

	Because by default, JS was conceptualized solely to the frontend.

	Why is NodeJS popular?

	Performance - NodeJS is one of the most performing environment for creating backend applications with JS.

	Familiarity - Since NodeJS is built-around and uses JS as its language, it is very familiar for most developers.

	NPM - Node Package Manager is the largest registry for node packages. Packages are bits of programs, methods, functions, codes that greatly help in the development of an application.

*/

// console.log("Hello, Node!");

// http://localhost:4000/

const http = require("http");

// require() = is a built-in JS method which allows us to import packages. Packages are pieces of code we can integrate into our application.

// "http" is a default package that comes with NodeJS. It allows us to use methods that let us create servers.

// http is now a module in your application that works much like an object. 



// console.log(http);

// The http module lets us create a server which is able to communicate with a client through the use of Hypertext Transfer Protocol (http)

// when you added the URL to the client (browser), the client made a request to the server, localhost:4000.
// Your server then gave a response, your message.

// http://localhost:4000/
// localhost - means your local server/machine
// 4000 - is the port/the process number in your computer

// When you added the url http://localhost:4000/, the client triggered the application running on localhost:4000, our NodeJS server.


// createServer() - is a method from the http module. It allows us to create a server that his able to handle the request of a client and send a response to the client.

// createServer() has a function as an argument. This function handles the request and the response. The request object contains details of the request from the client. The response object contains details of the response from the server. The createServer() method ALWAYS  receives the request object first before the response


http.createServer(function(request,response){

	// response.writeHead() - is a method of the response object. It allows us to add headers to our response. Headers are additional information about our response. We have 2 arguments in the writeHead() method, first is the http status code. 

	// An HTTP status is just a numerical code to tell the client about the status of their request. 200 means OK, HTTP 404 means the resource cannot be found. 

	// 'Content-Type' is one of the most recognizable headers. It simple pertains to the data type of the response.

	response.writeHead(200,{'Content-Type':'text/plain'});

	// response.end() it ends our response. Meaning there should be no more processes/tasks after reponse.end()
	// It is also able to send a message/data as a string.
	response.end("Hello from our First NodeJS Server!");
}).listen(4000);



/*
	listen() allows us to assign a port to our server. This will allow us to server/run our index.js server in our local machine assigned to port 4000.

	4000,4040,8000,5000,3000,4200.

*/


console.log("Server is running on localhost:4000!");





